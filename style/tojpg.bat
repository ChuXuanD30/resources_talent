@echo off
setlocal enabledelayedexpansion

REM 检查ImageMagick是否存在于系统的PATH环境变量中
where convert >nul 2>&1
if %errorlevel% neq 0 (
    echo ImageMagick's 'convert' command not found in PATH. Please install ImageMagick first.
    pause
    goto :eof
)

REM 遍历当前目录下所有PNG和WebP文件
for %%i in (*.png, *.webp) do (
    REM 获取原始文件名（不含扩展名）
    set "filename=%%~ni"
    REM 检查文件是否存在
    if exist "%%i" (
        REM 对于PNG文件
        if "%%~xi" == ".png" (
            REM 执行转换命令
            convert "%%i" "%%~ni.jpg"
            if !errorlevel! equ 0 (
                echo Successfully converted "%%i" to "%%~ni.jpg".
            ) else (
                echo Error converting "%%i" to JPEG. The original file remains untouched.
            )
        ) ELSEIF "%%~xi" == ".webp" (
            REM 执行转换命令，将WebP文件转换为JPG
            convert "%%i" "%%~ni.jpg"
            if !errorlevel! equ 0 (
                echo Successfully converted "%%i" to "%%~ni.jpg".
            ) else (
                echo Error converting "%%i" to JPEG. The original file remains untouched.
            )
        )
    ) ELSE (
        echo File "%%i" does not exist.
    )
)

echo All conversions finished.
pause